﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MoleculasBlock
{
    List<Molecula> Moleculas;

    public int Length()
    {
        return Moleculas.Count;
    }
    public MoleculasBlock(List<Molecula> lst)
    {
        Moleculas = lst;
    }
    public MoleculasBlock(int count)
    {
        Moleculas = new List<Molecula>(count);
    }
    public static MoleculasBlock operator +(MoleculasBlock mb, int a)
    {
        if(a > 0)
        {
            for (int i = 0; i < a; i++)
            {
                mb.Moleculas.Add(new Molecula(ElementType.Oxygen));
            }
        }
        else
        {
            mb.Moleculas.RemoveRange(mb.Moleculas.Count - 1 - a, a);
        }
        return new MoleculasBlock(0);
    }

}
[System.Serializable]
public class Molecula : MonoBehaviour
{
    [SerializeField] private ElementType _element;

    [SerializeField] private double _mass;

    public Molecula(ElementType type)
    {
        _element = type;
        _mass = ElementsTable.Table[_element];
    }

}
