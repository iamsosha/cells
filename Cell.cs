﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Cell : MonoBehaviour
{
    [SerializeField]
    private float[,] _moleculas;
    private Vector2 _polarization;

    public float[,] Moleculas { get => _moleculas; set => _moleculas = value; }

    public void Init(int n,int m)
    {
        Moleculas = new float[n,m];
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                _moleculas[i,j] = Random.Range(0,10);
            }
        }
    }

    void calcPolarization()
    {
        _polarization = Vector2.zero;
        for (int i = 0; i < _moleculas.GetLength(0); i++)
        {
            int len = _moleculas.GetLength(0);
            for (int j = 0; j < len/2; j++)
            {
                
                int k = ((len%2 == 1 && j == len/2 + 1)?0:(j < len/ 2 ? 1 : -1));
                _polarization.x += k*_moleculas[i,j];
            }

        }
        for (int i = 0; i < _moleculas.GetLength(0); i++)
        {
            int len = _moleculas.GetLength(0);
            for (int j = 0; j < len / 2; j++)
            {

                int k = ((len % 2 == 1 && j == len / 2 + 1) ? 0 : (j < len / 2 ? 1 : -1));
                _polarization.x += k * _moleculas[j,i];
            }

        }
    }

    void tryDevide()
    {
        
    }

    public static Cell operator *(Cell cell, float[,] op)
    {
        Cell newCell = new Cell();
        newCell.Moleculas = new float[op.GetLength(0), cell.Moleculas.GetLength(1)];
        for (int i = 0; i < op.GetLength(0); i++)
        {
            for (int j = 0; j < cell.Moleculas.GetLength(1); j++)
            {
                for (int k = 0; k < op.GetLength(0); k++)
                {
                    newCell.Moleculas[i,j] += op[i,k] * cell.Moleculas[k,j];
                }
            }
        }
        cell.Moleculas = newCell.Moleculas;
        return newCell;
    }


}
