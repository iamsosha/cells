﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ElementType
{
    Nitrogen,
    Oxygen,
    Ferrum
}

public class ElementsTable
{
    public static Dictionary<ElementType, double> Table = new Dictionary<ElementType, double> {
       { ElementType.Oxygen, 16},
       { ElementType.Nitrogen, 14},
       { ElementType.Ferrum, 56}
    };
}